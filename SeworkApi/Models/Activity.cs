﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SeworkApi.Models
{
    public partial class Activity
    {
        public Activity()
        {
            EnterpriseActivity = new HashSet<EnterpriseActivity>();
        }

        public int ActivityId { get; set; }
        public string ActivityName { get; set; }

        [JsonIgnore]
        public virtual ICollection<EnterpriseActivity> EnterpriseActivity { get; set; }
    }
}
