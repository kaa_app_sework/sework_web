﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SeworkApi.Models
{
    public partial class seworkdataContext : DbContext
    {
        public seworkdataContext()
        {
        }

        public seworkdataContext(DbContextOptions<seworkdataContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Activity> Activity { get; set; }
        public virtual DbSet<Enterprise> Enterprise { get; set; }
        public virtual DbSet<EnterpriseActivity> EnterpriseActivity { get; set; }
        public virtual DbSet<Region> Region { get; set; }

       

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<Activity>(entity =>
            {
                entity.Property(e => e.ActivityName)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Enterprise>(entity =>
            {
                entity.Property(e => e.EnterpriseName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Street)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.StreetNumber)
                    .IsRequired()
                    .HasMaxLength(5);

                entity.HasOne(d => d.Region)
                    .WithMany(p => p.Enterprise)
                    .HasForeignKey(d => d.RegionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Enterpris__Regio__60A75C0F");
            });

            modelBuilder.Entity<EnterpriseActivity>(entity =>
            {
                entity.HasOne(d => d.Activity)
                    .WithMany(p => p.EnterpriseActivity)
                    .HasForeignKey(d => d.ActivityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Enterpris__Activ__656C112C");

                entity.HasOne(d => d.Enterprise)
                    .WithMany(p => p.EnterpriseActivity)
                    .HasForeignKey(d => d.EnterpriseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Enterpris__Enter__6477ECF3");
            });

            modelBuilder.Entity<Region>(entity =>
            {
                entity.Property(e => e.RegionName)
                    .IsRequired()
                    .HasMaxLength(50);
            });
        }
    }
}
