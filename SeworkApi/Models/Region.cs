﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SeworkApi.Models
{
    public partial class Region
    {
        public Region()
        {
            Enterprise = new HashSet<Enterprise>();
        }

        public int RegionId { get; set; }
        public string RegionName { get; set; }

        [JsonIgnore]
        public virtual ICollection<Enterprise> Enterprise { get; set; }
    }
}
