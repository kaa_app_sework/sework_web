﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SeworkApi.Models
{
    public partial class EnterpriseActivity
    {
        public int EnterpriseId { get; set; }
        public int ActivityId { get; set; }
        public int EnterpriseActivityId { get; set; }

        public virtual Activity Activity { get; set; }
        [JsonIgnore]
        public virtual Enterprise Enterprise { get; set; }
    }
}
