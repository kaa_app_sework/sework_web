﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SeworkApi.Models
{
    public partial class Enterprise
    {
        public Enterprise()
        {
            EnterpriseActivity = new HashSet<EnterpriseActivity>();
        }

        public int EnterpriseId { get; set; }
        public string EnterpriseName { get; set; }
        public string Street { get; set; }
        public string StreetNumber { get; set; }
        public int PostalCode { get; set; }
        public int RegionId { get; set; }

        public virtual Region Region { get; set; }
        public virtual ICollection<EnterpriseActivity> EnterpriseActivity { get; set; }
    }
}
