using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeworkApi.Models;

namespace SeworkApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EnterpriseActivityController : ControllerBase
    {
        private readonly seworkdataContext _context;

        public EnterpriseActivityController(seworkdataContext context)
        {
            _context = context;
        }

        // GET: api/EnterpriseActivity
        [HttpGet]
        public async Task<ActionResult<IEnumerable<EnterpriseActivity>>> GetEnterpriseActivity()
        {
            return await _context.EnterpriseActivity.ToListAsync();
        }

        // GET: api/EnterpriseActivity/5
        [HttpGet("{id}")]
        public async Task<ActionResult<EnterpriseActivity>> GetEnterpriseActivity(int id)
        {
            var enterpriseActivity = await _context.EnterpriseActivity.FindAsync(id);

            if (enterpriseActivity == null)
            {
                return NotFound();
            }

            return enterpriseActivity;
        }

        // PUT: api/EnterpriseActivity/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEnterpriseActivity(int id, EnterpriseActivity enterpriseActivity)
        {
            if (id != enterpriseActivity.EnterpriseActivityId)
            {
                return BadRequest();
            }

            _context.Entry(enterpriseActivity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EnterpriseActivityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/EnterpriseActivity
        [HttpPost]
        public async Task<ActionResult<EnterpriseActivity>> PostEnterpriseActivity(EnterpriseActivity enterpriseActivity)
        {
            _context.EnterpriseActivity.Add(enterpriseActivity);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEnterpriseActivity", new { id = enterpriseActivity.EnterpriseActivityId }, enterpriseActivity);
        }

        // DELETE: api/EnterpriseActivity/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<EnterpriseActivity>> DeleteEnterpriseActivity(int id)
        {
            var enterpriseActivity = await _context.EnterpriseActivity.FindAsync(id);
            if (enterpriseActivity == null)
            {
                return NotFound();
            }

            _context.EnterpriseActivity.Remove(enterpriseActivity);
            await _context.SaveChangesAsync();

            return enterpriseActivity;
        }

        private bool EnterpriseActivityExists(int id)
        {
            return _context.EnterpriseActivity.Any(e => e.EnterpriseActivityId == id);
        }
    }
}
