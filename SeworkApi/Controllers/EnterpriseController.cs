using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeworkApi.Models;

namespace SeworkApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EnterpriseController : ControllerBase
    {
        private readonly seworkdataContext _context;

        public EnterpriseController(seworkdataContext context)
        {
            _context = context;
        }

        // GET: api/Enterprise
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Enterprise>>> GetEnterprise()
        {
            var seworkdataContext = _context.Enterprise
                .Include(e => e.Region)
                .Include(e => e.EnterpriseActivity)
                .ThenInclude(e => e.Activity)
                ;
            return await seworkdataContext.ToListAsync();
        }

        // GET: api/Enterprise/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Enterprise>> GetEnterprise(int id)
        {
            var enterprise = await _context.Enterprise
                .Include(e => e.Region)
                .Include(e => e.EnterpriseActivity)
                .ThenInclude(e => e.Activity)
                .FirstOrDefaultAsync(m => m.EnterpriseId == id);


            if (enterprise == null)
            {
                return NotFound();
            }
            
            return enterprise;
        }

        // PUT: api/Enterprise/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEnterprise(int id, Enterprise enterprise)
        {
            if (id != enterprise.EnterpriseId)
            {
                return BadRequest();
            }

            _context.Entry(enterprise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EnterpriseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Enterprise
        [HttpPost]
        public async Task<ActionResult<Enterprise>> PostEnterprise(Enterprise enterprise)
        {
            _context.Enterprise.Add(enterprise);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEnterprise", new { id = enterprise.EnterpriseId }, enterprise);
        }

        // DELETE: api/Enterprise/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Enterprise>> DeleteEnterprise(int id)
        {
            var enterprise = await _context.Enterprise.FindAsync(id);
            if (enterprise == null)
            {
                return NotFound();
            }

            _context.Enterprise.Remove(enterprise);
            await _context.SaveChangesAsync();

            return enterprise;
        }

        private bool EnterpriseExists(int id)
        {
            return _context.Enterprise.Any(e => e.EnterpriseId == id);
        }
    }
}
